class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.string :latitude
      t.string :longitude
      t.references :Route, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
