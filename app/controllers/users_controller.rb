class UsersController < ApplicationController
	before_action :authenticate_user! , except: [:landing]
  
  def test
    @user = User.first
  end
  
  def landing
  	if user_signed_in?
  		redirect_to users_test_path
  	else
  	  redirect_to new_user_session_path
  	end
  end
  
	def token
		@app = User.new()
 		respond_to do |format|
	    format.json {render json: @app.rest_client}
	    format.html {render json: @app.rest_client}
    end
	end

	def routing
		@app = User.new()
		respond_to do |format|
	    format.json {render json: @app.routing(params[:points],params[:accessToken])}
	    format.html {render json: @app.routing}
    end
	end

	def createTravel()
		@app = User.new()
		respond_to do |format|
	    format.json {render json: @app.savingTravel(params[:begin_point],params[:end_point],params[:name],params[:nickname],params[:accessToken])}
	    format.html {render json: @app.savingTravel(params[:begin_point],params[:end_point],params[:name],params[:nickname],params[:accessToken])}
    end
  end

  def getTravel()
  	@app = User.new()
  	respond_to do |format|
	    format.json {render json: @app.gettingTravel(params[:id])}
	    format.html {render json: @app.gettingTravel(params[:id])}
    end
  end
end
