class Point < ActiveRecord::Base
  belongs_to :Route
  def get_position
    @lat = self.latitude
    @long = self.longitude
    Hash["points[]#{self.id}","#{@lat},#{@long}"]
  end
end
