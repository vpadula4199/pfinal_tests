class Route < ActiveRecord::Base
  belongs_to :Travel
  has_many :points
end
