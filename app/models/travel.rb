class Travel < ActiveRecord::Base
  belongs_to :User
  has_many :routes
end
