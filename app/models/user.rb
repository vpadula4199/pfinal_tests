class User < ActiveRecord::Base
	has_many :travels
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

 	def rest_client()
		url = URI("https://api.intraffic.com.ve/oauth2/token")
		http = Net::HTTP.new(url.host, url.port)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		request = Net::HTTP::Post.new(url)
		request["content-type"] = 'application/json'
		request["authorization"] = 'Basic YWNhZGVtaWFoYWNrOjQzNTFkNDBmMzAzMGFhZDIyMDdmZTU3MTUyMjk4MTFiNzFjN2Y2ZWU='
		request["cache-control"] = 'no-cache'
		request.body = "{\n    \"grant_type\": \"client_credentials\",\n    \"dataType\": \"json\",\n    \"contentType\": \"application/json\",\n    \"async\": \"true\",\n    \"cache\": \"false\"\n}"
		response = http.request(request)
		return response.read_body
	end

	def routing(pointsArray,token)
		param = Hash[*pointsArray]
		url = URI("https://api.intraffic.com.ve/routing.geojson")
		url.query = URI.encode_www_form(param)
		http = Net::HTTP.new(url.host, url.port)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		request = Net::HTTP::Get.new(url)
		request["user-agent"] = 'develop'
		request["authorization"] = "Bearer #{token}"
		request["cache-control"] = 'no-cache'
		request["compact"] = true
		response = http.request(request)
		return response.read_body
	end

	def savingTravel(begin_point,end_point,name,nickname,token)
		uri = URI("https://api.intraffic.com.ve/travels/new.json")
		req = Net::HTTP::Post.new(uri)
		req.set_form_data('name' => "#{name}", 'begin_point' => "#{begin_point}",'end_point'=> "#{end_point}",'nickname'=> "#{nickname}")
		req["user-agent"] = 'develop'
		req["authorization"] = "Bearer #{token}"
		req["cache-control"] = 'no-cache'
		http = Net::HTTP.new(uri.host, uri.port)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		http.start do |h|
		  return h.request(req).body
		end
	end

	def gettingTravel(travel_id,token)
		url = URI("https://api.intraffic/pois/#{travel_id}")
		http = Net::HTTP.new(url.host, url.port)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		request = Net::HTTP::Get.new(url)
		request["user-agent"] = 'develop'
		request["authorization"] = "Bearer 60ff8cd374a5db40efdd91ac27a8e0bddab4c6ea"
		request["cache-control"] = 'no-cache'
		response = http.request(request)
		return response.read_body
	end
end
