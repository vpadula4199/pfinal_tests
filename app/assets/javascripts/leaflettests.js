// Class below will be deleted, Its integrated in the object Traveluser()
class Viaje {
  constructor(p1, p2) {
    this.p1 = p1;
    this.p2 = p2;
    this.puntos = [];
  }
}

class TravelUser {
  constructor(p1,p2) {
  	// this.id;
   //  this.name;
   //  this.nickname;
    this.origin;
    this.destination;
    this.points = [];
    this.distance = 0;
    this.freeTravelTime = 0;
    this.travelTime = 0;
    this.speed = 0;
    this.gjsonlayer;
    this.gjson;
    this.reset = function () {
	    // this.id;
    	// this.name;
    	// this.nickname;
	    this.origin = null;
	    this.destination = null;
	    this.points = [];
	    this.distances = 0;
	    this.freeTravelTime = 0;
	    this.travelTime = 0;
	    this.speed = 0;
	    this.gjsonlayer;
	    this.gjson;
  	}
  }
  
}

var viaj = new Viaje()
var travel = new TravelUser(); 
var mymap = settingMap();
// Variables below will be deleted... They are integrated in the object TravelUser()
var origin,dataRoute,dest,gjson,token,distance,freeTravelTime,travelTime,speed,gjsonlayer;

function settingMap(){
  var mymap = L.map('mapid').setView([10.19228, -64.67626], 13);
  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'solidxnake.0mi2boia',
    accessToken:'pk.eyJ1Ijoic29saWR4bmFrZSIsImEiOiJjaXFyOTNhZG8wMnJpZm5ubnd4cjRnNXEyIn0.0WFH0DH90t62ij51ZSKhww'
  }).addTo(mymap);
  mymap.on('click', onMapClick);
  mymap.on('contextmenu', contextMenuEvent);
  return mymap
}

getToken();
setInterval(getToken,1500000)

function contextMenuEvent(e){
 clearMap()
}



function getToken(){
  $.ajax({
    url: '/token',
    dataType: 'json',
    success: (data) => {
      token = data.access_token
      console.log(token)
    }
  });
}



function dragEnd2(e){
  beforeRenderMarkers()
}

function beforeRenderMarkers(){

	var markersCoord = []

	if(travel.origin != null){
		markersCoord.push(travel.origin.getLatLng())
	}
	
	if(travel.points.length != 0){
		$.each(travel.points,function(index,value){
			markersCoord.push(value.getLatLng())
		});
	}

	if(travel.destination != null){
		markersCoord.push(travel.destination.getLatLng())
	}

	settingRoute(markersCoord)
} 

function settingGeojson(){
    travel.gjsonlayer = L.geoJson(gjson, {
          style: function(feature) {
            var traffic = (feature.properties.rt_travel_time)/(feature.properties.free_travel_time)
            
            if(traffic <= 1.3){
              return {color: "green",weight:5,opacity:1};
            }else if(traffic <=1.7){
              return {color: "orange",weight:5,opacity:1};
            }else if(traffic > 1.7){
              return {color: "red",weight:5,opacity:1};
            }
          }
  }).addTo(mymap);
}

var redMarker = L.ExtraMarkers.icon({
    icon: 'fa-bullseye',
    markerColor: 'green-light',
    shape: 'star',
    prefix: 'fa'
  });

function additionalMarker(pointNumber){
 var additionalMarker = L.ExtraMarkers.icon({
				    icon: 'fa-number',
				    number:pointNumber.toString(),
				    markerColor: 'blue-dark',
				    shape: 'square',
				    prefix: 'fa'
				  });

 return additionalMarker
}
var flagMarker = L.ExtraMarkers.icon({
    icon: 'fa-flag-checkered',
    markerColor: 'cyan',
    shape: 'square',
    prefix: 'fa'
  });


function onMapClick(e) {

	console.log(e.latlng)

	if (travel.origin == null) {
		travel.origin = (L.marker(e.latlng,{icon:redMarker,draggable:true}).on({click:removeMarker,dragend:dragEnd2})).addTo(mymap)
		alert("Punto De Origen")
	} else if(travel.destination == null){
		travel.destination = (L.marker(e.latlng,{icon:flagMarker,draggable:true}).on({click:removeMarker,dragend:dragEnd2})).addTo(mymap);
		alert("Punto De Destino")
		beforeRenderMarkers();
	} else{
		travel.points.push((L.marker(e.latlng,{icon:additionalMarker((travel.points.length + 1)),draggable:true}).on({click:removeMarker,dragend:dragEnd2})).addTo(mymap))
		alert("Puntos Adicionales")
		beforeRenderMarkers();
	}
}

function removeMarker(e){
 	e.target.bindPopup("Saludos")
  // mymap.removeLayer(e.target)
  // $.each(viaj.puntos, function(index,value){
  //   if (value === e.target.getLatLng()){
  //     viaj.puntos.splice(index-2,1)
  //   }
  // })
  // if (viaj.puntos.length == 0){
  //   counter = 0
  //   viaj.p1 = null
  //   viaj.p2 = null
  //   mymap.removeLayer(origin)
  //   mymap.removeLayer(dest)
  // } 
}

function settingRoute(params){
    if(travel.gjsonlayer!=null){
      mymap.removeLayer(travel.gjsonlayer)
    }
    var coord=[]
    $.each(params,function(index,value){
      coord.push('points[]'+(index+1))
      coord.push(value.lng+','+value.lat)
    });
    console.log(coord);
    $.ajax({
    url: '/routing',
    dataType: 'json',
    method:'POST',
    data: {points:coord,
           accessToken:token
          },
    success: (data) => {
      console.log(data)
      gjson = data
      travel.gjson = data
      gettingTravelData()
      settingGeojson()
    }
  });
}

function gettingTravelData(){
  distance = 0;
  freeTravelTime = 0;
  travelTime = 0;
  speed = 0;
  if (gjson === undefined || gjson === null || gjson.length == 0) {
    return 'No Information Available'
  } else {  
    gjson.features.forEach(
      function(element, index, array){
      distance += element.properties.distance;
      freeTravelTime += element.properties.free_travel_time;
      travelTime += element.properties.rt_travel_time;
      speed += parseInt(element.properties.speed);    
    });
    return getTravelData();
  }
}


function getTravelData(){
	// distance -> Km, freeTravelTime and travelTime -> Minutes, speed -> Km/H
	var dataRoute = {
				distance: (distance/1000).toFixed(2),
				freeTravelTime: (freeTravelTime/60).toFixed(2),
				travelTime: (travelTime/60).toFixed(2),
				speed: (speed/(gjson.features.length)).toFixed(2)
	}
	console.log(dataRoute)
	return dataRoute
}

function clearMap(){
  mymap.removeLayer(travel.origin);
  mymap.removeLayer(travel.destination);
  mymap.removeLayer(travel.gjsonlayer);
  if(travel.points.length!=0){
    $.each(travel.points,function(index,value){
      mymap.removeLayer(value);
    });
  }
  console.log(travel.reset)
  travel.reset();
  console.log(travel)
}


// Function not ready due to the fact that the endpoint is not available
// function getUserTravels(){
// 	$.ajax({
// 		url:'/travels',
// 		dataType:'json',
// 		method:'GET',
// 		success:(data)=>{
// 			return data
// 		}
// 	})
// }

function postUserTravel(name,nickname){
	$.ajax({
		url:'/travels',
		dataType:'json',
		method:'POST',
		data: {
						begin_point:travel.origin,
						end_point:travel.destination,
						name:name,
						nickname:nickname,
						accessToken:token
					},
		success:(data) =>{
			console.log(data)
		}
	});
}

function getOriginDest(){
  var origin = travel.origin.getLatLng();
  var destination = travel.destination.getLatLng();
  clearMap();
  return [origin,destination];
}

function newTravel(){

  if(travel.origin != null){
    console.log("Hola")
    clearMap()
  };

  mymap.on('click', onMapClick);
}

function createTravel(travelName,orig,destiny,rData){
  return {
    name: travelName,
    begin_point:orig,
    end_point:destiny,
    routeData:rData
  };
}

// Function not available... Server answer with 500 internal error
// function getTravelInfo(){
// 	$.ajax({
// 	  url: '/travel//info',
// 	  dataType: 'json',
// 	  success: (data) => {
// 	    console.log(data)
// 	  }
// 	});
// }

// Function that have to be called when an user clicks the travel
function settingTravel(params){
  
  if(travel.origin != null){
    clearMap()
  };
  
  travel.origin = (L.marker([params[0].lat,params[0].lng],{draggable:true}).on({click:removeMarker,dragend:dragEnd2})).bindPopup("Inicio").addTo(mymap)
  travel.destination = (L.marker([params[1].lat,params[1].lng],{draggable:true}).on({click:removeMarker,dragend:dragEnd2})).bindPopup("Inicio").addTo(mymap)
  beforeRenderMarkers();
  mymap.off('click', onMapClick);
}
