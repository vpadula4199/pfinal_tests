class LandingContainer extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      username: "elieser"
    }  
  }

  render () {
    console.log(this.state.username);
    return (
      <div className="LandingContainer">
      <LandingHeader/>
      <LandingLoQueSea/>
      <LandingForm/>
      <LandingEslogan/>
      <LandingFooter/>
        {this.state.username}
      </div>
      );
  }
}
