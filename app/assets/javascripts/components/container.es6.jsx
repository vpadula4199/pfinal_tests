const state = {
  viajes:[
  {
  "id": "0e6f7125-b9b4-4460-b7a8-93b6e8d60da1",
  "name": "VIAJE DE PRUEBA 1",
  "begin_point": {lng:-64.6959114074707,
                  lat:10.19904151877366},
  "end_point": {lng:-64.69428062438965,
                lat:10.196422800340647},
  rutas:[],
  routeData: {distance: "0.71", freeTravelTime: "0.72", travelTime: "12.93", speed: "13.43"}
  },
  {
  "id": "0e6f7125-b9b4-4460-b7a8-93b6e8d60da1",
  "name": "VIAJE DE PRUEBA 2",
  "begin_point":  {lng:-64.69573974609375,
                lat:10.188904424588001},
  "end_point":  {lng:-64.6933364868164,
                lat:10.186201145016888},
  rutas:[],
  routeData:"empty"
  }  
  ]};
const defaultstate = "empty";

class Container extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      data: state,
      currentTravel:0,
      currentRoute:0,
      test: "empty"
    }  
  }
  _changeCurrentTravel(newCurrent){
    this.setState({currentTravel:parseInt(newCurrent)});
    //this.forceUpdate();
  }
  _changeCurrentRoute(newCurrent){
    this.setState({currentRoute:parseInt(newCurrent)});
    //this.forceUpdate();
  }
  _fillState(newTravel){
    this.setState({data: {viajes: this.state.data.viajes.concat(newTravel)}});
  }
  _fillTravelData(newData){
    console.log(newData);
    this.state.data.viajes[this.state.currentTravel].routeData = newData;
    this.forceUpdate();
    console.log(this.state);
  }
  render () {
    //console.log(this.state.data.viajes);
    return (
      <div className="Contenedor">
        <OptionContainer fillState={this._fillState.bind(this)} tests={this._fillTravelData.bind(this)}/>
        <UserInfo 
          data={this.state}
          _changeCurrentRoute={this._changeCurrentRoute.bind(this)}
          _changeCurrentTravel={this._changeCurrentTravel.bind(this)}
        />
      </div>
      );
  }
}

