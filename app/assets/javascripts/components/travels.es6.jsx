class Travels extends React.Component {
  render () {
     let travelNodes = this.props.data.viajes.map((req,index)=>{
          return <Travel 
            data={req} 
            key={index} 
            clicked={this.props.clicked} 
            value={index}
            open={this.props.currentTravel == index ? true:false}
            />
        })
      return (
      <div className="col-md-4 Travels">
        <div className="travels-label text-center">
          <p>Viajes</p>
        </div>
        {travelNodes}
      </div>
      );
  }
}

