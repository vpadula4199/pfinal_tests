class OptionContainer extends React.Component {
/* Esperando que este el enpoint de las rutas para incluir esto en el render
        <div className="col-md-5">
          <CreateRoute/>
        </div>
        <div className="col-md-1 option-otro">
          <a className="btn btn-info">Otro</a>
        </div>*/

  _handleClickClearMap(e){
    e.preventDefault();
    window.clearMap();
  } 
  _handleClickNewTravel(e){
    e.preventDefault();
    window.newTravel();
  }

  render () {
    return (
      <div className="row option-container">
        <div className="col-md-4 create-travel">
          <CreateTravel createAndSaveTravel={this.props.fillState}/>
        </div>
        <div className="col-md-2 clear-map">
          <a className="btn btn-info" onClick={this._handleClickClearMap.bind(this)}>
          <span className="glyphicon glyphicon-refresh" aria-hidden="true"></span> Limpiar Mapa</a>
        </div>
        <div className="col-md-1 new-travel">
          <a className="btn btn-info" onClick={this._handleClickNewTravel.bind(this)}>
          <span className="glyphicon glyphicon-refresh" aria-hidden="true"></span> Nuevo Viaje</a>
        </div> 
      </div>);
  }
}

