class RouteInfo extends React.Component {
  render () {
    return (
      <div className="col-md-4 Route-Info text-center">
          <div className="routeinfo-label">
            <p>Informacion</p>
          </div>
          <div className="route-specs">
            <p>DISTANCIA KM</p>
            <p>{this.props.data.routeData.distance}</p>
          </div>
          <div className="route-specs">
            <p>VELOCIDAD PROMEDIO KM/H</p>
            <p>{this.props.data.routeData.speed}</p>
          </div>
          <div className="route-specs">
            <p>TIEMPO DE VIAJE  min </p>
            <p>{this.props.data.routeData.travelTime}</p>
          </div>
      </div>
      );
  }
}

