class Travel extends React.Component {

  handleClick(e){
    e.preventDefault();
    this.props.clicked(this.props.value);
    window.settingTravel([this.props.data.begin_point,this.props.data.end_point]);
  }
  render () {
    let open = <h3>{this.props.data.name}</h3>;
    if (this.props.open) {
      open = <div>
        <h3>{this.props.data.name}</h3>
        <p>{this.props.data.begin_point.lng+','+this.props.data.begin_point.lat}</p>
        <p>{this.props.data.end_point.lng+','+this.props.data.end_point.lat}</p>
      </div>;

    }
    return (
      <div className="Travel">
        <a href="#" onClick={this.handleClick.bind(this)}>
          {open}
        </a>
      </div>
      );
  }
}

