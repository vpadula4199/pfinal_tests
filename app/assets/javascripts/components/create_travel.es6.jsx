class CreateTravel extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      value: ''
    }

  }
  _llamarfunciondeafuera(e){
    e.preventDefault();
    //console.log([this.state.value,window.getOriginDest(),window.gettingTravelData()]);
    if (this.state.value == '') {
      alert('Ingrese el nombre del Viaje');
    }else{
      let data = window.gettingTravelData();
      let name = this.state.value;
      let coords = window.getOriginDest();
      let begin = coords[0];
      let end = coords[1];
      console.log('react:');
      let Travel = window.createTravel(name,begin,end,data)
      this.props.createAndSaveTravel(Travel);
      //console.log(Travel);
      this.setState({value: ''});
    }
  }
  _handleChange(e){
    e.preventDefault();
    newState = e.target.value;
    this.setState({value: newState});
    //console.log(this.state.value == '');
    //console.log('updated');
  }
  render () {
    return (
      <div className="input-group">
        <input type="text" 
          placeholder="Nombre del Viaje" 
          value={this.state.value} 
          onChange={this._handleChange.bind(this)} 
          className="form-control"/>
        <a onClick={this._llamarfunciondeafuera.bind(this)} className="btn btn-danger input-group-addon">
        <span className="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar Viaje</a>
      </div>);
  }
}

