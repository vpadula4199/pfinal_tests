class TravelRoute extends React.Component {
  handleClick(e){
    e.preventDefault();
    this.props.clicked(this.props.value)
  }
  render () {
    return (
      <div className={this.props.current ? "Route Route-selected":"Route"} >
        <a href="#" onClick={this.handleClick.bind(this)}>
          <h3>{this.props.data.name}</h3>
        </a>
      </div>
      );
  }
}

