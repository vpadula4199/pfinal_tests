class UserInfo extends React.Component {


  _components(){
    if(this.props.data == "empty"){
      return (
        <div className="UserInfo row">
          <h3>El usuario no tiene Viajes/Rutas asociadas</h3>
        </div>);
    } else {
      return (
        <div className="UserInfo row">
          <Travels 
                data={this.props.data.data} 
                clicked={this.props._changeCurrentTravel.bind(this)}
                currentTravel={this.props.data.currentTravel}/>
          <TravelRoutes/>
          <RouteInfo data={this.props.data.data.viajes[this.props.data.currentTravel]}/>

        </div>);
    };

  }
  render () {
/*    let components = {
      if (this.props.data.viajes == []) {
        <Travels 
          data={this.props.data} 
          clicked={this._changeCurrentTravel.bind(this)}
          currentTravel={this.state.currentTravel}
        />
      } else  {
        <div/>
      }

        <TravelRoutes 
          data={this.props.data.viajes[this.state.currentTravel].rutas} 
          clicked={this._changeCurrentRoute.bind(this)} 
          currentRoute={this.state.currentRoute}
        />
        <RouteInfo data={this.props.data.viajes[this.state.currentTravel].rutas[this.state.currentRoute]}/>
    }*/
    return this._components();
  }
}

